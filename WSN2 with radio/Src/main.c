
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "arm_math.h"
#include "radio.h"
#include "hw.h"

#define LOCAL_LOGGING	true

/* Generic defines */

typedef enum{
	ACCEL_X,
	ACCEL_Y,
	ACCEL_Z,
	PIEZO_X,
	PIEZO_Y,
	TEMPSTORE
} sensors_t;

#define ADC_FREQUENCY				20485			
#define TOTAL_SAMPLES				1024																			/* 8192 for 2s of data at 4kHz */

#define FFT_LENGTH					64 																				/* supported FFT Lengths are 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 */
#define FFT_SAMPLES					(FFT_LENGTH * 2)

#define FFT_NUMS						(uint16_t)(TOTAL_SAMPLES / FFT_SAMPLES)

#define VADC								2.0																				/* ADC reference voltage */

const float hannValues[] =																						/* Cosine values for Hann window on 128 samples */
{
    0.0000000000F,0.0012235838F,0.0048913408F,0.0109942955F,0.0195175129F,0.0304401352F,0.0437354330F,0.0593708704F,
    0.0773081849F,0.0975034809F,0.1199073370F,0.1444649280F,0.1711161560F,0.1997958020F,0.2304336810F,0.2629548180F,
    0.2972796290F,0.3333241140F,0.3710000660F,0.4102152870F,0.4508738100F,0.4928761360F,0.5361194800F,0.5804980170F,
    0.6259031450F,0.6722237520F,0.7193464810F,0.7671560170F,0.8155353620F,0.8643661220F,0.9135288010F,0.9629030890F,
    1.0123681600F,1.0618029600F,1.1110865300F,1.1600982400F,1.2087181700F,1.2568273300F,1.3043079800F,1.3510439500F,
    1.3969208500F,1.4418264300F,1.4856507700F,1.5282866500F,1.5696297300F,1.6095788200F,1.6480361700F,1.6849076700F,
    1.7201030900F,1.7535362900F,1.7851254600F,1.8147933000F,1.8424672100F,1.8680794500F,1.8915673600F,1.9128734500F,
    1.9319455900F,1.9487371100F,1.9632069000F,1.9753195700F,1.9850454600F,1.9923607900F,1.9972476400F,1.9996940600F,
    1.9996940600F,1.9972476400F,1.9923607900F,1.9850454600F,1.9753195700F,1.9632069000F,1.9487371100F,1.9319455900F,
    1.9128734500F,1.8915673600F,1.8680794500F,1.8424672100F,1.8147933000F,1.7851254600F,1.7535362900F,1.7201030900F,
    1.6849076700F,1.6480361700F,1.6095788200F,1.5696297300F,1.5282866500F,1.4856507700F,1.4418264300F,1.3969208500F,
    1.3510439500F,1.3043079800F,1.2568273300F,1.2087181700F,1.1600982400F,1.1110865300F,1.0618029600F,1.0123681600F,
    0.9629030890F,0.9135288010F,0.8643661220F,0.8155353620F,0.7671560170F,0.7193464810F,0.6722237520F,0.6259031450F,
    0.5804980170F,0.5361194800F,0.4928761360F,0.4508738100F,0.4102152870F,0.3710000660F,0.3333241140F,0.2972796290F,
    0.2629548180F,0.2304336810F,0.1997958020F,0.1711161560F,0.1444649280F,0.1199073370F,0.0975034809F,0.0773081849F,
    0.0593708704F,0.0437354330F,0.0304401352F,0.0195175129F,0.0109942955F,0.0048913408F,0.0012235838F,0.0000000000F
};

typedef struct 
{
	uint16_t mean[3];																	/* x, y, z mean value of raw data in bit */
	float32_t rms[3];																	/* x, y, z rms in G (accelerometer and piezo only) */
	float32_t peak[3];																/* x, y, z peak value in G (accelerometer and piezo), C (temperature) or V (storage) */
	float32_t fft_avg[FFT_LENGTH];										/* fft output in psd (accelerometer and piezo only) */
} wsn_data_t;


/* Accelerometer */

#define ACCEL_NUM						3																					/* X, Y, Z data */
#define SAMPLES_PER_DFSDM		10 																				/* number of samples required for single dfsdm output = filter order * (oversampling + 1) + 1 */

#define FREQ_SPACING				(ADC_FREQUENCY / (FFT_SAMPLES * SAMPLES_PER_DFSDM))

#define ACCEL_ADC_BUFFER		(2 * ACCEL_NUM * SAMPLES_PER_DFSDM)
#define DFSDM_DMA_BUFFER		128
#define DFSDM_BIT_SHIFT			11	

#define DFSDM_BITS					32767.0																		/* 15 bits dfsdm output (+1 unused sign bit) */
#define ACCEL_V_PER_LSB			(VADC/DFSDM_BITS)

#define ADXL_V_PER_G				(6.5E-3 * (VADC / 3.0))										/* value from ADXL377 datahseet at 3V supply */
#define KIONIX_V_PER_G			(33E-3 * (VADC / 3.3))										/* value from Kionix KX220-1072 */			  								

#define RADIO_ACCEL_SCALING	(65536.0 / 200.0)													/* convert data from 200G full scale to 16 bits */	
#define RADIO_PSD_SCALING		(65536.0 / 100.0)														


/* Piezo and temperature sensor */

#define SENSOR_NUM					2																					/* X & Y data for piezo or temperature & storage voltage*/	
#define SENSORS_DMA_BUFFER	(SENSOR_NUM * TOTAL_SAMPLES) 			

#define SENSORS_ADC_MAX			32768																			/* 15 bits full scale */
#define SENSORS_V_PER_LSB		(VADC/SENSORS_ADC_MAX)	

#define PIEZO_V_PER_G				(1)
#define PIEZO_G_SCALING			(SENSORS_V_PER_LSB / PIEZO_V_PER_G)
#define PIEZO_FFT_G_SCALING	(PIEZO_G_SCALING / FFT_LENGTH)		

#define THERMISTOR_RREF			10000.0																		/* value of fixed ref resistor in thermistor cct */
#define TEMP_MAX						100.0

#define PIEZO_SCALING				(65536.0 / 2.0)		
#define TEMP_SCALING				(65536.0 / (TEMP_MAX * 2))								/* convert data from +/- 100C to 16 bits */
#define STORAGE_SCALING			(65536.0 / 2.0)														/* convert data from 2.0V max to 16 bits */

typedef struct
{
	double	Coeff_A;
	double	Coeff_B;
	double	Coeff_C;
	uint32_t	MaxRes;
	uint32_t	MinRes;
}THERMISTOR_CORRECTION_T;

/* Panasonic ERTJ1VR103FA R25=10K, B25/50=4250 */ 
/* 0C = 35.8K 25C=10K 70C=1.518K */
/* A=1.315332E-03, B=2.13357E-04 C=9.4E-08 */
/* 125C =253R -40C = 431K */
static const THERMISTOR_CORRECTION_T Thermistor_Corrections[] = {1.315332E-03,2.13357E-04,9.4E-08,431000,253};


/* Radio */

#define RF_FREQUENCY                                868000000 // Hz

#define TX_OUTPUT_POWER                             14        // dBm

#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       7         // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         0         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

#define RX_TIMEOUT_VALUE                            1000
#define RADIO_BUFFER_SIZE                           64 				// Define the payload size here
#define LED_PERIOD_MS               								200

typedef struct 
{
	uint32_t dev_id;																	/* device id */
	uint32_t timer;																		/* time in seconds from power-on */
	sensors_t sensor;																	/* sensor measured */
	uint16_t mean[3];																	/* x, y, z mean value of raw data in bit */
	uint16_t rms[3];																	/* 16-bit scaled x, y, z rms value (accelerometer and piezo only) */
	uint16_t peak[3];																	/* 16-bit scaled x, y, z peak value */
	uint16_t fft_avg[FFT_LENGTH];											/* 16-bit scaled fft output in psd (accelerometer and piezo only) */
	uint8_t fft_scaling;
} radio_data_t;

#define RADIO_DATA_SIZE															156

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
DMA_HandleTypeDef hdma_adc1;
DMA_HandleTypeDef hdma_adc2;

DFSDM_Filter_HandleTypeDef hdfsdm1_filter0;
DFSDM_Filter_HandleTypeDef hdfsdm1_filter1;
DFSDM_Filter_HandleTypeDef hdfsdm1_filter2;
DFSDM_Filter_HandleTypeDef hdfsdm1_filter3;
DFSDM_Channel_HandleTypeDef hdfsdm1_channel0;
DFSDM_Channel_HandleTypeDef hdfsdm1_channel1;
DFSDM_Channel_HandleTypeDef hdfsdm1_channel2;
DFSDM_Channel_HandleTypeDef hdfsdm1_channel3;
DMA_HandleTypeDef hdma_dfsdm1_flt0;
DMA_HandleTypeDef hdma_dfsdm1_flt1;
DMA_HandleTypeDef hdma_dfsdm1_flt2;
DMA_HandleTypeDef hdma_dfsdm1_flt3;

UART_HandleTypeDef hlpuart1;

QSPI_HandleTypeDef hqspi;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi2;
SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim15;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

static uint16_t pg_accel_adc_buffer[ACCEL_ADC_BUFFER];				/* circular DMA buffer for accelerometer ADC data*/ 
	
static int32_t pg_filter1_dma[DFSDM_DMA_BUFFER];							/* dfsdm output buffers, must be int32 */
static int32_t pg_filter2_dma[DFSDM_DMA_BUFFER];
static int32_t pg_filter3_dma[DFSDM_DMA_BUFFER];

static int16_t pg_filter_outputx[TOTAL_SAMPLES]; 							/* x, y and z data in uint16 */
static int16_t pg_filter_outputy[TOTAL_SAMPLES]; 
static int16_t pg_filter_outputz[TOTAL_SAMPLES]; 

static uint16_t pg_dfsdm1_counter;
static uint16_t pg_dfsdm2_counter;
static uint16_t pg_dfsdm3_counter;

static uint16_t pg_measurement_buffer[SENSORS_DMA_BUFFER];		/* DMA buffer for piezo, temperature and storage ADC data */		

static bool pg_start_measuring = false;
static bool pg_measurement_done = false;

static RadioEvents_t pg_RadioEvents;
static uint16_t pg_radio_bufferSize = RADIO_BUFFER_SIZE;
static uint8_t pg_radio_buffer[RADIO_BUFFER_SIZE];

static bool pg_received;

static uint32_t pg_timer;
static uint32_t pg_dev_id;

static float32_t pg_accel_g_scaling;
static float32_t pg_fft_g_scaling;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_DFSDM1_Init(void);
static void MX_LPUART1_UART_Init(void);
static void MX_QUADSPI_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI3_Init(void);
static void MX_TIM15_Init(void);
static void MX_RTC_Init(void);
static void MX_ADC2_Init(void);
static void MX_TIM3_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* Retargets the C library printf function to the USART. */
/* is syscalls.c required forthis to work? */
#define PUTCHAR_PROTOTYPE
int fputc(int ch, FILE *f)
	PUTCHAR_PROTOTYPE  
{
    HAL_UART_Transmit(&hlpuart1, (uint8_t *)&ch, 1, 100);
    return ch;
}

void OnTxDone( void );
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );
void OnTxTimeout( void );
void OnRxTimeout( void );
void OnRxError( void );

static HAL_StatusTypeDef start_piezo(void);
static void stop_piezo(void);
static wsn_data_t process_piezo(sensors_t sensor);

static HAL_StatusTypeDef start_accelerometer(void);
static void stop_accelerometer(void);
static wsn_data_t process_accelerometer(sensors_t sensor);

static HAL_StatusTypeDef start_temp_storage(void);
static void stop_temp_storage(void);
static wsn_data_t process_temp_storage(void);
static float Thermistor_RToC(float res);

static void sendmessage(wsn_data_t message, sensors_t sensor);
static void convert_16_to_8bit(uint16_t in, uint8_t *out);
static void convert_32_to_8bit(uint32_t in, uint8_t *out);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
	uint16_t i;

	wsn_data_t wsn_data;
	sensors_t state;

	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_DFSDM1_Init();
  MX_LPUART1_UART_Init();
  MX_QUADSPI_Init();
  MX_SPI2_Init();
  MX_SPI3_Init();
  MX_TIM15_Init();
  MX_RTC_Init();
  MX_ADC2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	 
	 // Radio initialization
  pg_RadioEvents.TxDone = OnTxDone;
  pg_RadioEvents.RxDone = OnRxDone;
  pg_RadioEvents.TxTimeout = OnTxTimeout;
  pg_RadioEvents.RxTimeout = OnRxTimeout;
  pg_RadioEvents.RxError = OnRxError;

  Radio.IoInit( );
  HW_SPI_Init( );
  HW_RTC_Init( );

	pg_dev_id = HAL_GetUIDw0();

	printf("******** \r\n");
	printf("* WSN2 * \r\n");
	printf("******** \r\n");
	
	HAL_Delay((uint32_t)(pg_dev_id % 5000)); // "random" startup delay to avoid data corruption when WSN's are transmitting simultaneously
	HAL_TIM_OC_Start_IT(&htim3, TIM_CHANNEL_3);
	
	pg_start_measuring = true;
	state = ACCEL_X;
	
	/* read version bit 0 to know which accelerometer is present */
	if(HAL_GPIO_ReadPin(VERSION_BIT0_GPIO_Port, VERSION_BIT0_Pin))
	{
		pg_accel_g_scaling = ACCEL_V_PER_LSB / KIONIX_V_PER_G;
	}
	else
	{
		pg_accel_g_scaling = ACCEL_V_PER_LSB / ADXL_V_PER_G;
	}
	pg_fft_g_scaling	= pg_accel_g_scaling / FFT_LENGTH;
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		switch(state)
		{
			case 	ACCEL_X:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_accelerometer();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if((pg_dfsdm1_counter >= TOTAL_SAMPLES) && (pg_dfsdm2_counter >= TOTAL_SAMPLES) && (pg_dfsdm3_counter >= TOTAL_SAMPLES))
				{
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_accelerometer();	
					
					wsn_data = process_accelerometer(ACCEL_X);	
					sendmessage(wsn_data, state);		
					state = ACCEL_Y;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			case ACCEL_Y:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_accelerometer();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if((pg_dfsdm1_counter >= TOTAL_SAMPLES) && (pg_dfsdm2_counter >= TOTAL_SAMPLES) && (pg_dfsdm3_counter >= TOTAL_SAMPLES))
				{
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_accelerometer();		
					
					wsn_data = process_accelerometer(ACCEL_Y);	
					sendmessage(wsn_data, state);		
					state = ACCEL_Z;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			case ACCEL_Z:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_accelerometer();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if((pg_dfsdm1_counter >= TOTAL_SAMPLES) && (pg_dfsdm2_counter >= TOTAL_SAMPLES) && (pg_dfsdm3_counter >= TOTAL_SAMPLES))
				{
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_accelerometer();		
					
					wsn_data = process_accelerometer(ACCEL_Z);	
					sendmessage(wsn_data, state);		
					state = PIEZO_X;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			case PIEZO_X:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_piezo();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if(pg_measurement_done)
				{
					pg_measurement_done = false;
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_piezo();
					
					for(i = 0; i < TOTAL_SAMPLES; i++)
					{
						pg_filter_outputx[i] = (int16_t)pg_measurement_buffer[i*2];
						pg_filter_outputy[i] = (int16_t)pg_measurement_buffer[i*2+1];
						pg_filter_outputz[i] = 0U;
					}		
					
					wsn_data = process_piezo(PIEZO_X);	
					sendmessage(wsn_data, state);		
					state = PIEZO_Y;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			case PIEZO_Y:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_piezo();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if(pg_measurement_done)
				{
					pg_measurement_done = false;
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_piezo();
					
					for(i = 0; i < TOTAL_SAMPLES; i++)
					{
						pg_filter_outputx[i] = (int16_t)pg_measurement_buffer[i*2];
						pg_filter_outputy[i] = (int16_t)pg_measurement_buffer[i*2+1];
						pg_filter_outputz[i] = 0U;
					}			
					
					wsn_data = process_piezo(PIEZO_Y);	
					sendmessage(wsn_data, state);		
					state = TEMPSTORE;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			case TEMPSTORE:
				if(pg_start_measuring)
				{
					pg_start_measuring = false;
					start_temp_storage();
					HAL_TIM_OC_Start(&htim15, TIM_CHANNEL_1);  
				}
				else if(pg_measurement_done)
				{
					pg_measurement_done = false;
					HAL_TIM_OC_Stop(&htim15, TIM_CHANNEL_1);   
					stop_temp_storage();
					
					wsn_data = process_temp_storage();					
					sendmessage(wsn_data, state);		
					state = ACCEL_X;
				}	
				else
				{
					HAL_Delay(1);
				}
				break;
			default:
				break;
		}

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure LSE Drive Capability 
    */
  HAL_PWR_EnableBkUpAccess();

  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LPUART1
                              |RCC_PERIPHCLK_DFSDM1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_MultiModeTypeDef multimode;
  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 3;
  hadc1.Init.DiscontinuousConvMode = ENABLE;
  hadc1.Init.NbrOfDiscConversion = 3;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIG_T15_TRGO;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = ENABLE;
  hadc1.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_16;
  hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_1;
  hadc1.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
  hadc1.Init.Oversampling.OversamplingStopReset = ADC_REGOVERSAMPLING_CONTINUED_MODE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the ADC multi-mode 
    */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_12CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* ADC2 init function */
static void MX_ADC2_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.NbrOfConversion = 2;
  hadc2.Init.DiscontinuousConvMode = ENABLE;
  hadc2.Init.NbrOfDiscConversion = 2;
  hadc2.Init.ExternalTrigConv = ADC_EXTERNALTRIG_T15_TRGO;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc2.Init.OversamplingMode = ENABLE;
  hadc2.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_16;
  hadc2.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_1;
  hadc2.Init.Oversampling.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
  hadc2.Init.Oversampling.OversamplingStopReset = ADC_REGOVERSAMPLING_CONTINUED_MODE;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* DFSDM1 init function */
static void MX_DFSDM1_Init(void)
{

  hdfsdm1_filter0.Instance = DFSDM1_Filter0;
  hdfsdm1_filter0.Init.RegularParam.Trigger = DFSDM_FILTER_SW_TRIGGER;
  hdfsdm1_filter0.Init.RegularParam.FastMode = DISABLE;
  hdfsdm1_filter0.Init.RegularParam.DmaMode = ENABLE;
  hdfsdm1_filter0.Init.FilterParam.SincOrder = DFSDM_FILTER_SINC1_ORDER;
  hdfsdm1_filter0.Init.FilterParam.Oversampling = 998;
  hdfsdm1_filter0.Init.FilterParam.IntOversampling = 1;
  if (HAL_DFSDM_FilterInit(&hdfsdm1_filter0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_filter1.Instance = DFSDM1_Filter1;
  hdfsdm1_filter1.Init.RegularParam.Trigger = DFSDM_FILTER_SW_TRIGGER;
  hdfsdm1_filter1.Init.RegularParam.FastMode = DISABLE;
  hdfsdm1_filter1.Init.RegularParam.DmaMode = ENABLE;
  hdfsdm1_filter1.Init.FilterParam.SincOrder = DFSDM_FILTER_SINC1_ORDER;
  hdfsdm1_filter1.Init.FilterParam.Oversampling = 8;
  hdfsdm1_filter1.Init.FilterParam.IntOversampling = 1;
  if (HAL_DFSDM_FilterInit(&hdfsdm1_filter1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_filter2.Instance = DFSDM1_Filter2;
  hdfsdm1_filter2.Init.RegularParam.Trigger = DFSDM_FILTER_SW_TRIGGER;
  hdfsdm1_filter2.Init.RegularParam.FastMode = DISABLE;
  hdfsdm1_filter2.Init.RegularParam.DmaMode = ENABLE;
  hdfsdm1_filter2.Init.FilterParam.SincOrder = DFSDM_FILTER_SINC1_ORDER;
  hdfsdm1_filter2.Init.FilterParam.Oversampling = 8;
  hdfsdm1_filter2.Init.FilterParam.IntOversampling = 1;
  if (HAL_DFSDM_FilterInit(&hdfsdm1_filter2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_filter3.Instance = DFSDM1_Filter3;
  hdfsdm1_filter3.Init.RegularParam.Trigger = DFSDM_FILTER_SW_TRIGGER;
  hdfsdm1_filter3.Init.RegularParam.FastMode = DISABLE;
  hdfsdm1_filter3.Init.RegularParam.DmaMode = ENABLE;
  hdfsdm1_filter3.Init.FilterParam.SincOrder = DFSDM_FILTER_SINC1_ORDER;
  hdfsdm1_filter3.Init.FilterParam.Oversampling = 8;
  hdfsdm1_filter3.Init.FilterParam.IntOversampling = 1;
  if (HAL_DFSDM_FilterInit(&hdfsdm1_filter3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_channel0.Instance = DFSDM1_Channel0;
  hdfsdm1_channel0.Init.OutputClock.Activation = DISABLE;
  hdfsdm1_channel0.Init.OutputClock.Selection = DFSDM_CHANNEL_OUTPUT_CLOCK_SYSTEM;
  hdfsdm1_channel0.Init.OutputClock.Divider = 2;
  hdfsdm1_channel0.Init.Input.Multiplexer = DFSDM_CHANNEL_INTERNAL_REGISTER;
  hdfsdm1_channel0.Init.Input.DataPacking = DFSDM_CHANNEL_STANDARD_MODE;
  hdfsdm1_channel0.Init.Input.Pins = DFSDM_CHANNEL_SAME_CHANNEL_PINS;
  hdfsdm1_channel0.Init.SerialInterface.Type = DFSDM_CHANNEL_SPI_RISING;
  hdfsdm1_channel0.Init.SerialInterface.SpiClock = DFSDM_CHANNEL_SPI_CLOCK_EXTERNAL;
  hdfsdm1_channel0.Init.Awd.FilterOrder = DFSDM_CHANNEL_FASTSINC_ORDER;
  hdfsdm1_channel0.Init.Awd.Oversampling = 1;
  hdfsdm1_channel0.Init.Offset = 0x00;
  hdfsdm1_channel0.Init.RightBitShift = 0x00;
  if (HAL_DFSDM_ChannelInit(&hdfsdm1_channel0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_channel1.Instance = DFSDM1_Channel1;
  hdfsdm1_channel1.Init.OutputClock.Activation = DISABLE;
  hdfsdm1_channel1.Init.OutputClock.Selection = DFSDM_CHANNEL_OUTPUT_CLOCK_SYSTEM;
  hdfsdm1_channel1.Init.OutputClock.Divider = 2;
  hdfsdm1_channel1.Init.Input.Multiplexer = DFSDM_CHANNEL_INTERNAL_REGISTER;
  hdfsdm1_channel1.Init.Input.DataPacking = DFSDM_CHANNEL_STANDARD_MODE;
  hdfsdm1_channel1.Init.Input.Pins = DFSDM_CHANNEL_SAME_CHANNEL_PINS;
  hdfsdm1_channel1.Init.SerialInterface.Type = DFSDM_CHANNEL_SPI_RISING;
  hdfsdm1_channel1.Init.SerialInterface.SpiClock = DFSDM_CHANNEL_SPI_CLOCK_EXTERNAL;
  hdfsdm1_channel1.Init.Awd.FilterOrder = DFSDM_CHANNEL_FASTSINC_ORDER;
  hdfsdm1_channel1.Init.Awd.Oversampling = 1;
  hdfsdm1_channel1.Init.Offset = 0x00;
  hdfsdm1_channel1.Init.RightBitShift = 0x00;
  if (HAL_DFSDM_ChannelInit(&hdfsdm1_channel1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_channel2.Instance = DFSDM1_Channel2;
  hdfsdm1_channel2.Init.OutputClock.Activation = DISABLE;
  hdfsdm1_channel2.Init.OutputClock.Selection = DFSDM_CHANNEL_OUTPUT_CLOCK_SYSTEM;
  hdfsdm1_channel2.Init.OutputClock.Divider = 2;
  hdfsdm1_channel2.Init.Input.Multiplexer = DFSDM_CHANNEL_INTERNAL_REGISTER;
  hdfsdm1_channel2.Init.Input.DataPacking = DFSDM_CHANNEL_STANDARD_MODE;
  hdfsdm1_channel2.Init.Input.Pins = DFSDM_CHANNEL_SAME_CHANNEL_PINS;
  hdfsdm1_channel2.Init.SerialInterface.Type = DFSDM_CHANNEL_SPI_RISING;
  hdfsdm1_channel2.Init.SerialInterface.SpiClock = DFSDM_CHANNEL_SPI_CLOCK_EXTERNAL;
  hdfsdm1_channel2.Init.Awd.FilterOrder = DFSDM_CHANNEL_FASTSINC_ORDER;
  hdfsdm1_channel2.Init.Awd.Oversampling = 1;
  hdfsdm1_channel2.Init.Offset = 0x00;
  hdfsdm1_channel2.Init.RightBitShift = 0x00;
  if (HAL_DFSDM_ChannelInit(&hdfsdm1_channel2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  hdfsdm1_channel3.Instance = DFSDM1_Channel3;
  hdfsdm1_channel3.Init.OutputClock.Activation = DISABLE;
  hdfsdm1_channel3.Init.OutputClock.Selection = DFSDM_CHANNEL_OUTPUT_CLOCK_SYSTEM;
  hdfsdm1_channel3.Init.OutputClock.Divider = 2;
  hdfsdm1_channel3.Init.Input.Multiplexer = DFSDM_CHANNEL_INTERNAL_REGISTER;
  hdfsdm1_channel3.Init.Input.DataPacking = DFSDM_CHANNEL_STANDARD_MODE;
  hdfsdm1_channel3.Init.Input.Pins = DFSDM_CHANNEL_SAME_CHANNEL_PINS;
  hdfsdm1_channel3.Init.SerialInterface.Type = DFSDM_CHANNEL_SPI_RISING;
  hdfsdm1_channel3.Init.SerialInterface.SpiClock = DFSDM_CHANNEL_SPI_CLOCK_EXTERNAL;
  hdfsdm1_channel3.Init.Awd.FilterOrder = DFSDM_CHANNEL_FASTSINC_ORDER;
  hdfsdm1_channel3.Init.Awd.Oversampling = 1;
  hdfsdm1_channel3.Init.Offset = 0x00;
  hdfsdm1_channel3.Init.RightBitShift = 0x00;
  if (HAL_DFSDM_ChannelInit(&hdfsdm1_channel3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_DFSDM_FilterConfigRegChannel(&hdfsdm1_filter0, DFSDM_CHANNEL_0, DFSDM_CONTINUOUS_CONV_ON) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_DFSDM_FilterConfigRegChannel(&hdfsdm1_filter1, DFSDM_CHANNEL_1, DFSDM_CONTINUOUS_CONV_ON) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_DFSDM_FilterConfigRegChannel(&hdfsdm1_filter2, DFSDM_CHANNEL_2, DFSDM_CONTINUOUS_CONV_ON) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_DFSDM_FilterConfigRegChannel(&hdfsdm1_filter3, DFSDM_CHANNEL_3, DFSDM_CONTINUOUS_CONV_ON) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* LPUART1 init function */
static void MX_LPUART1_UART_Init(void)
{

  hlpuart1.Instance = LPUART1;
  hlpuart1.Init.BaudRate = 115200;
  hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
  hlpuart1.Init.StopBits = UART_STOPBITS_1;
  hlpuart1.Init.Parity = UART_PARITY_NONE;
  hlpuart1.Init.Mode = UART_MODE_TX_RX;
  hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&hlpuart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* QUADSPI init function */
static void MX_QUADSPI_Init(void)
{

  /* QUADSPI parameter configuration*/
  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 255;
  hqspi.Init.FifoThreshold = 1;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 1;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* RTC init function */
static void MX_RTC_Init(void)
{

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI2 init function */
static void MX_SPI2_Init(void)
{

  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI3 init function */
static void MX_SPI3_Init(void)
{

  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi3.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 31752;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 10000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM15 init function */
static void MX_TIM15_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim15.Instance = TIM15;
  htim15.Init.Prescaler = 0;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 1550;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim15) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim15, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_OC_Init(&htim15) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_OC_ConfigChannel(&htim15, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim15, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(nTHERM_EN_GPIO_Port, nTHERM_EN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, nPIEZO_EN_Pin|nLORA_EN_Pin|nADXL372_EN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MEM_EN_Pin|AVDD_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, VMON_EN_Pin|LED2_Pin|LED1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, nLORA_CS_Pin|nLORA_RESET_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(nADXL372_CS_GPIO_Port, nADXL372_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : nTHERM_EN_Pin VMON_EN_Pin LED2_Pin LED1_Pin */
  GPIO_InitStruct.Pin = nTHERM_EN_Pin|VMON_EN_Pin|LED2_Pin|LED1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : nPIEZO_EN_Pin nLORA_CS_Pin nLORA_RESET_Pin nLORA_EN_Pin 
                           nADXL372_EN_Pin */
  GPIO_InitStruct.Pin = nPIEZO_EN_Pin|nLORA_CS_Pin|nLORA_RESET_Pin|nLORA_EN_Pin 
                          |nADXL372_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PGOOD_2V2_Pin LORA_BUSY_Pin */
  GPIO_InitStruct.Pin = PGOOD_2V2_Pin|LORA_BUSY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MEM_EN_Pin AVDD_EN_Pin */
  GPIO_InitStruct.Pin = MEM_EN_Pin|AVDD_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : HARV_DETECT_Pin ADXL372_IRQ1_Pin ADXL372_IRQ2_Pin */
  GPIO_InitStruct.Pin = HARV_DETECT_Pin|ADXL372_IRQ1_Pin|ADXL372_IRQ2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : VERSION_BIT2_Pin */
  GPIO_InitStruct.Pin = VERSION_BIT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(VERSION_BIT2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LORA_DIO1_Pin */
  GPIO_InitStruct.Pin = LORA_DIO1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LORA_DIO1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : nADXL372_CS_Pin */
  GPIO_InitStruct.Pin = nADXL372_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(nADXL372_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : VERSION_BIT1_Pin VERSION_BIT0_Pin */
  GPIO_InitStruct.Pin = VERSION_BIT1_Pin|VERSION_BIT0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == LORA_DIO1_Pin)
	{
		Radio.IrqProcess();
	}
}

/*accelerometer functions */

static HAL_StatusTypeDef start_accelerometer(void)
{
	HAL_StatusTypeDef status = HAL_ERROR;;
	
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_SET);
	HAL_Delay(10);
	
	if((HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter1, pg_filter1_dma, DFSDM_DMA_BUFFER) != HAL_OK)|
		(HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter2, pg_filter2_dma, DFSDM_DMA_BUFFER) != HAL_OK)|
		(HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter3, pg_filter3_dma, DFSDM_DMA_BUFFER) != HAL_OK))
	{
		printf("DFSM start error");
	}
	else
	{
		if (HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED) != HAL_OK)
		{
			printf("ADC calibration error");
		}
		else
		{		
			if (HAL_ADC_Start_DMA(&hadc1, (uint32_t*)pg_accel_adc_buffer, ACCEL_ADC_BUFFER) != HAL_OK)
			{
				printf("ADC start error");
			}
			else
			{
				status = HAL_OK;
			}
		}
	}
	
	return status;
}	

static void stop_accelerometer(void)
{
	HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter1);
	HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter2);
	HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter3);

	HAL_ADC_Stop_DMA(&hadc1);
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_RESET);
			
	pg_dfsdm1_counter = 0;
	pg_dfsdm2_counter = 0;
	pg_dfsdm3_counter = 0;
}

static wsn_data_t process_accelerometer(sensors_t sensor)
{
	wsn_data_t wsn_data = {0};
	uint16_t i, j;
		
	uint32_t mean_x = 0, mean_y = 0, mean_z = 0;
	uint64_t rms_x = 0, rms_y = 0, rms_z = 0;
	uint16_t peak_x = 0, peak_y = 0, peak_z = 0;
	uint16_t peaklocation_x, peaklocation_y, peaklocation_z;
	
	arm_rfft_fast_instance_f32 rfft;
	float32_t rfft_in[FFT_SAMPLES], rfft_out[FFT_SAMPLES], rfft_mag[FFT_LENGTH], fft_avg[FFT_LENGTH];
			
	/* calculate mean value of all samples */
	for(i = 0; i < TOTAL_SAMPLES; i++)
	{
		mean_x += pg_filter_outputx[i];		
		mean_y += pg_filter_outputy[i];	
		mean_z += pg_filter_outputz[i];					
	}
	mean_x = mean_x / TOTAL_SAMPLES;
	mean_y = mean_y / TOTAL_SAMPLES;
	mean_z = mean_z / TOTAL_SAMPLES;
			
	/* subtract mean from every sample, find peak and RMS value */
	for(i = 0; i < TOTAL_SAMPLES; i++)
	{
		pg_filter_outputx[i] = pg_filter_outputx[i] - mean_x;
		pg_filter_outputy[i] = pg_filter_outputy[i] - mean_y;
		pg_filter_outputz[i] = pg_filter_outputz[i] - mean_z;

		if ((abs(pg_filter_outputx[i])) > peak_x)
		{
			peak_x = abs(pg_filter_outputx[i]);
			peaklocation_x = i;
		}
		if ((abs(pg_filter_outputy[i])) > peak_y)
		{
			peak_y = abs(pg_filter_outputy[i]);
			peaklocation_y = i;
		}
		if ((abs(pg_filter_outputz[i])) > peak_z)
		{
			peak_z = abs(pg_filter_outputz[i]);
			peaklocation_z = i;
		}
				
		rms_x += pg_filter_outputx[i] * pg_filter_outputx[i];		
		rms_y += pg_filter_outputy[i] * pg_filter_outputy[i];	
		rms_z += pg_filter_outputz[i] * pg_filter_outputz[i];					
	}

	wsn_data.mean[0] = (uint16_t)mean_x;
	wsn_data.mean[1] = (uint16_t)mean_y;
	wsn_data.mean[2] = (uint16_t)mean_z;
			
	wsn_data.peak[0] = (float32_t)peak_x * pg_accel_g_scaling;			
	wsn_data.peak[1] = (float32_t)peak_y * pg_accel_g_scaling;			
	wsn_data.peak[2] = (float32_t)peak_z * pg_accel_g_scaling;	
				
	wsn_data.rms[0] = sqrt((float32_t)rms_x/TOTAL_SAMPLES) * pg_accel_g_scaling;
	wsn_data.rms[1] = sqrt((float32_t)rms_y/TOTAL_SAMPLES) * pg_accel_g_scaling;
	wsn_data.rms[2] = sqrt((float32_t)rms_z/TOTAL_SAMPLES) * pg_accel_g_scaling;
	
	/* fft */
	if (arm_rfft_fast_init_f32(&rfft, FFT_SAMPLES) == ARM_MATH_SUCCESS)
	{
		for(j = 0; j < FFT_NUMS; j++)
		{				
			for(i = 0; i < FFT_SAMPLES; i++)
			{			
				if(sensor == ACCEL_X)
				{					
					rfft_in[i] = (float_t)pg_filter_outputx[i + j * FFT_SAMPLES];	
				}
				else if(sensor == ACCEL_Y)
				{					
					rfft_in[i] = (float_t)pg_filter_outputy[i + j * FFT_SAMPLES];	
				}
				else
				{					
					rfft_in[i] = (float_t)pg_filter_outputz[i + j * FFT_SAMPLES];	
				}
				
				/* Hanning window on 128 samples */
				if(FFT_SAMPLES == 128)
				{
					rfft_in[i] = rfft_in[i] * hannValues[i];
				}
			}
					
			arm_rfft_fast_f32(&rfft, rfft_in, rfft_out, 0U);
			/* Data is represented in the FFT using N/2 complex numbers. These are packed into the output array in alternating real and imaginary components:
			X = { real[0], imag[0], real[1], imag[1], real[2], imag[2] ... real[(N/2)-1], imag[(N/2)-1 } */				
			arm_cmplx_mag_f32(rfft_out+2, rfft_mag+1, FFT_LENGTH-1); 
			rfft_mag[0] = rfft_out[0]; // DC component

			for(i = 0; i < FFT_LENGTH; i++)
			{
				fft_avg[i] += rfft_mag[i];
			}
		}
				
		for(i = 0; i < FFT_LENGTH; i++)
		{
			fft_avg[i] = (fft_avg[i] / FFT_NUMS) * pg_fft_g_scaling;
			fft_avg[i] = ((fft_avg[i] * fft_avg[i]) / (2 * FREQ_SPACING)); // convert to PSD
			wsn_data.fft_avg[i] = fft_avg[i];
		}
	}
	else
	{
		printf("arm_rfft_fast_init_f32 error\r\n");
	}
			
	return wsn_data;
}

/* piezo functions */

static HAL_StatusTypeDef start_piezo(void)
{
	HAL_StatusTypeDef status = HAL_ERROR;
	ADC_ChannelConfTypeDef sConfig;
	
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(nPIEZO_EN_GPIO_Port, nPIEZO_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
	
	if (HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED) != HAL_OK)
	{
		printf("ADC calibration error");
	}
	else
	{	
		if (HAL_ADC_Start_DMA(&hadc2, (uint32_t*)pg_measurement_buffer, SENSORS_DMA_BUFFER) != HAL_OK)
		{
			printf("ADC start error");
		}
		else
		{
			status = HAL_OK;
		}
	}	
	
	return status;
}

static void stop_piezo(void)
{
	HAL_ADC_Stop_DMA(&hadc2);		
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(nPIEZO_EN_GPIO_Port, nPIEZO_EN_Pin, GPIO_PIN_SET);
}

static wsn_data_t process_piezo(sensors_t sensor)
{
	wsn_data_t wsn_data = {0};
	uint16_t i, j;
	
	uint32_t mean_x = 0, mean_y = 0;
	uint64_t rms_x = 0, rms_y = 0;
	uint16_t peak_x = 0, peak_y = 0;
	uint16_t peaklocation_x, peaklocation_y;
	
	arm_rfft_fast_instance_f32 rfft;
	float32_t rfft_in[FFT_SAMPLES], rfft_out[FFT_SAMPLES], rfft_mag[FFT_LENGTH], fft_avg[FFT_LENGTH];
			
	/* calculate mean value of all samples */
	for(i = 0; i < TOTAL_SAMPLES; i++)
	{
		mean_x += pg_filter_outputx[i];		
		mean_y += pg_filter_outputy[i];					
	}
	mean_x = mean_x / TOTAL_SAMPLES;
	mean_y = mean_y / TOTAL_SAMPLES;
			
	/* subtract mean from every sample, find peak and RMS value */
	for(i = 0; i < TOTAL_SAMPLES; i++)
	{
		pg_filter_outputx[i] = pg_filter_outputx[i] - mean_x;
		pg_filter_outputy[i] = pg_filter_outputy[i] - mean_y;

		if ((abs(pg_filter_outputx[i])) > peak_x)
		{
			peak_x = abs(pg_filter_outputx[i]);
			peaklocation_x = i;
		}
		if ((abs(pg_filter_outputy[i])) > peak_y)
		{
			peak_y = abs(pg_filter_outputy[i]);
			peaklocation_y = i;
		}
				
		rms_x += pg_filter_outputx[i] * pg_filter_outputx[i];		
		rms_y += pg_filter_outputy[i] * pg_filter_outputy[i];					
	}

	wsn_data.mean[0] = (uint16_t)mean_x;
	wsn_data.mean[1] = (uint16_t)mean_y;

	wsn_data.peak[0] = (float32_t)peak_x * PIEZO_G_SCALING;			
	wsn_data.peak[1] = (float32_t)peak_y * PIEZO_G_SCALING;			
				
	wsn_data.rms[0] = sqrt((float32_t)rms_x/TOTAL_SAMPLES) * PIEZO_G_SCALING;
	wsn_data.rms[1] = sqrt((float32_t)rms_y/TOTAL_SAMPLES) * PIEZO_G_SCALING;
	
	/* fft */
	if (arm_rfft_fast_init_f32(&rfft, FFT_SAMPLES) == ARM_MATH_SUCCESS)
	{
		for(j = 0; j < FFT_NUMS; j++)
		{				
			for(i = 0; i < FFT_SAMPLES; i++)
			{			
				if(sensor == PIEZO_X)
				{					
					rfft_in[i] = (float_t)pg_filter_outputx[i + j * FFT_SAMPLES];	
				}
				else 
				{					
					rfft_in[i] = (float_t)pg_filter_outputy[i + j * FFT_SAMPLES];	
				}
				
				/* Hanning window on 128 samples */
				if(FFT_SAMPLES == 128)
				{
					rfft_in[i] = rfft_in[i] * hannValues[i];
				}
			}
					
			arm_rfft_fast_f32(&rfft, rfft_in, rfft_out, 0U);
			/* Data is represented in the FFT using N/2 complex numbers. These are packed into the output array in alternating real and imaginary components:
			X = { real[0], imag[0], real[1], imag[1], real[2], imag[2] ... real[(N/2)-1], imag[(N/2)-1 } */				
			arm_cmplx_mag_f32(rfft_out+2, rfft_mag+1, FFT_LENGTH-1); 
			rfft_mag[0] = rfft_out[0]; // DC component

			for(i = 0; i < FFT_LENGTH; i++)
			{
				fft_avg[i] += rfft_mag[i];
			}
		}
				
		for(i = 0; i < FFT_LENGTH; i++)
		{
			fft_avg[i] = (fft_avg[i] / FFT_NUMS) * PIEZO_FFT_G_SCALING;
			wsn_data.fft_avg[i] = fft_avg[i];
		}
	}
	else
	{
		printf("arm_rfft_fast_init_f32 error\r\n");
	}
			
	return wsn_data;
}

/* temperature and storage measurement functions */

static HAL_StatusTypeDef start_temp_storage(void)
{
	HAL_StatusTypeDef status = HAL_ERROR;
	ADC_ChannelConfTypeDef sConfig;
	
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(nTHERM_EN_GPIO_Port, nTHERM_EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(5);
	
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
	
	if (HAL_ADCEx_Calibration_Start(&hadc2, ADC_SINGLE_ENDED) != HAL_OK)
	{
		printf("ADC calibration error");
	}
	else
	{	
		if (HAL_ADC_Start_DMA(&hadc2, (uint32_t*)pg_measurement_buffer, SENSOR_NUM) != HAL_OK)
		{
			printf("ADC start error");
		}
		else
		{
			status = HAL_OK;
		}
	}	
		
	return status;
}

static void stop_temp_storage(void)
{
	HAL_ADC_Stop_DMA(&hadc2);		
	HAL_GPIO_WritePin(AVDD_EN_GPIO_Port, AVDD_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(nTHERM_EN_GPIO_Port, nTHERM_EN_Pin, GPIO_PIN_SET);
}

static wsn_data_t process_temp_storage(void)
{
	wsn_data_t wsn_data = {0};	
	double v1 = 0.0; 
	
	if (HAL_GPIO_ReadPin(PGOOD_2V2_GPIO_Port, PGOOD_2V2_Pin) == GPIO_PIN_SET)
	{
		wsn_data.rms[1] = 1.0;
	}
 else
	{
		wsn_data.rms[1] = 0.0;
	}
					
	wsn_data.mean[0] = pg_measurement_buffer[0];
	wsn_data.mean[1] = pg_measurement_buffer[1];
	
	v1 = SENSORS_ADC_MAX / (float)pg_measurement_buffer[0];				/* calc ratio of top resistor over bottom resistor */
	v1 = THERMISTOR_RREF / (v1-1); 																/* calc thermistor resistance */
	
	wsn_data.peak[0] = Thermistor_RToC(v1);												/* Convert resistance to temperature */
	wsn_data.peak[1] = wsn_data.mean[1] * SENSORS_V_PER_LSB;
	
	return wsn_data;
}

static float Thermistor_RToC(float res)
{
	float a,b,c,lnr;
	
	lnr = logf(res);
	//lnr=log(res);

	a=Thermistor_Corrections->Coeff_A;
	b=lnr*Thermistor_Corrections->Coeff_B;
	c=(pow(lnr,3))*Thermistor_Corrections->Coeff_C;

	a=1/(a+b+c);

	return a-(float)273.15;
}

/* radio functions */

static void sendmessage(wsn_data_t message, sensors_t sensor)
{
	radio_data_t radio_data = {0};
	
	uint8_t buffer[RADIO_DATA_SIZE];
	uint8_t i;
	
	float32_t psd_peak = 0.0;
	uint8_t psd_scaling = 1;
	
	if(LOCAL_LOGGING)
	{
		printf("%d, %d, %d, %d, %d, %d, %f, %f, %f, %f, %f, %f, ", pg_dev_id, sensor, pg_timer, message.mean[0], message.mean[1], message.mean[2], 
			message.rms[0], message.rms[1], message.rms[2], message.peak[0], message.peak[1], message.peak[2]);
		for(i = 0; i < FFT_LENGTH; i++)
		{
			printf("%f, ", message.fft_avg[i]);
		}
		printf("\r\n");
	}
	
	HAL_GPIO_WritePin(nLORA_EN_GPIO_Port, nLORA_EN_Pin, GPIO_PIN_RESET);
	
	Radio.Init( &pg_RadioEvents );
  Radio.SetChannel( RF_FREQUENCY );

  Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                 LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

  Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, false );
	
	radio_data.dev_id = pg_dev_id;
	radio_data.timer = pg_timer;
	radio_data.sensor = sensor;
	radio_data.mean[0] = message.mean[0];
	radio_data.mean[1] = message.mean[1];
	radio_data.mean[2] = message.mean[2];
	
	/* convert data to 16-bit unsigned values */
	if ((sensor == ACCEL_X) || (sensor == ACCEL_Y) || (sensor == ACCEL_Z))
	{
		radio_data.rms[0] = (uint16_t)(message.rms[0] * RADIO_ACCEL_SCALING);
		radio_data.rms[1] = (uint16_t)(message.rms[1] * RADIO_ACCEL_SCALING);
		radio_data.rms[2] = (uint16_t)(message.rms[2] * RADIO_ACCEL_SCALING);
		radio_data.peak[0] =(uint16_t)(message.peak[0] * RADIO_ACCEL_SCALING);
		radio_data.peak[1] =(uint16_t)(message.peak[1] * RADIO_ACCEL_SCALING);
		radio_data.peak[2] =(uint16_t)(message.peak[2] * RADIO_ACCEL_SCALING);
		
		/* PSD max = 100 PSD, resolution = 1 uPSD. This has to be converted to a 16-bit value for tranmission.
		The PSD data will be multiplied by 10 a number of times (max 16 times) to get the most of the 16-bit range */
		for(i = 0; i < FFT_LENGTH; i++)
		{			
			if(message.fft_avg[i] > psd_peak)
			{
				psd_peak = message.fft_avg[i];
			}
		}
		
		i = 0;
		while (((uint16_t)psd_peak < 10) && (i < 0x0F))
		{
			i++;
			psd_peak = psd_peak * 10;
			psd_scaling = psd_scaling * 10;
		}
		radio_data.fft_scaling = i;
		
		for(i = 0; i < FFT_LENGTH; i++)
		{
			radio_data.fft_avg[i] = (uint16_t)(message.fft_avg[i] * psd_scaling * RADIO_PSD_SCALING);
		}
	}
	else if ((sensor == PIEZO_X) || (sensor == PIEZO_Y))
	{
		radio_data.rms[0] = (uint16_t)(message.rms[0] * PIEZO_SCALING);
		radio_data.rms[1] = (uint16_t)(message.rms[1] * PIEZO_SCALING);
		radio_data.rms[2] = (uint16_t)(message.rms[2] * PIEZO_SCALING);
		radio_data.peak[0] =(uint16_t)(message.peak[0] * PIEZO_SCALING);
		radio_data.peak[1] =(uint16_t)(message.peak[1] * PIEZO_SCALING);
		radio_data.peak[2] =(uint16_t)(message.peak[2] * PIEZO_SCALING);
		
		for(i = 0; i < FFT_LENGTH; i++)
		{
			radio_data.fft_avg[i] = (uint16_t)(message.fft_avg[i] * PIEZO_SCALING);
		}
		
	}
	else
	{
		radio_data.peak[0] = (uint16_t)((message.peak[0] + TEMP_MAX) * TEMP_SCALING);
		radio_data.peak[1] = (uint16_t)(message.peak[1] * STORAGE_SCALING);
		radio_data.rms[1] = (uint16_t)message.rms[1]; /* PGood boolean */
	}
	
	/* split into bytes */
	convert_32_to_8bit(radio_data.dev_id, buffer);
	convert_32_to_8bit(radio_data.timer, buffer+4);
	buffer[8] = radio_data.sensor;
	convert_16_to_8bit(radio_data.mean[0], buffer + 9);
	convert_16_to_8bit(radio_data.mean[1], buffer + 11);
	convert_16_to_8bit(radio_data.mean[2], buffer + 13);
	convert_16_to_8bit(radio_data.rms[0], buffer + 15);
	convert_16_to_8bit(radio_data.rms[1], buffer + 17);
	convert_16_to_8bit(radio_data.rms[2], buffer + 19);
	convert_16_to_8bit(radio_data.peak[0], buffer + 21);
	convert_16_to_8bit(radio_data.peak[1], buffer + 23);
	convert_16_to_8bit(radio_data.peak[2], buffer + 25);
	for(i = 0; i < FFT_LENGTH; i++)
	{
		convert_16_to_8bit(radio_data.fft_avg[i], buffer + (27 + i*2));
	}
	buffer[155] = radio_data.fft_scaling; 
	
  Radio.Send(buffer, RADIO_DATA_SIZE);
}

static void convert_16_to_8bit(uint16_t in, uint8_t *out)
{
  out[0] = (uint8_t)( (in >> 8) & 0xFF);
  out[1] = (uint8_t)(in & 0xFF);
}

static void convert_32_to_8bit(uint32_t in, uint8_t *out)
{
	out[0] = (uint8_t)( (in >> 24) & 0xFF);
  out[1] = (uint8_t)( (in >> 16) & 0xFF);
  out[2] = (uint8_t)( (in >> 8) & 0xFF);
  out[3] = (uint8_t)(in & 0xFF);
}

/* interrupt handlers */

void OnTxDone( void )
{		
	//HAL_GPIO_WritePin(nLORA_CS_GPIO_Port, nLORA_CS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(nLORA_EN_GPIO_Port, nLORA_EN_Pin, GPIO_PIN_SET);
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	int i;
	
  pg_radio_bufferSize = size;
  memcpy( pg_radio_buffer, payload, pg_radio_bufferSize );
	
	printf("\r\nReceived %d bytes\r\n", size);
	
	for(i = 0; i < pg_radio_bufferSize; i++)
	{
		printf("%c", pg_radio_buffer[i]);
	}
	printf("\n");
	
	pg_received = true;
}

void OnTxTimeout( void )
{
	printf("\r\nTx Timeout\r\n");
}

void OnRxTimeout( void )
{
	printf("\r\nRx Timeout\r\n");
}

void OnRxError( void )
{
	printf("\r\nRx Error\r\n");
}

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
	pg_timer += 10;
	pg_start_measuring = true; 
}

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* AdcHandle)
{			
	int32_t i;

	if (AdcHandle == &hadc1)
	{	
		for(i = 0; i < (ACCEL_ADC_BUFFER / 2); i = i + 3)
		{
			//hdfsdm1_channel0.Instance->CHDATINR = (int16_t)pg_adc_buffer[i];
			hdfsdm1_channel1.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i];
			hdfsdm1_channel2.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i+1];
			hdfsdm1_channel3.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i+2];
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{			
	int32_t i;

	if (AdcHandle == &hadc1)
	{
		for(i = (ACCEL_ADC_BUFFER / 2); i < ACCEL_ADC_BUFFER; i = i + 3)
		{
			//hdfsdm1_channel0.Instance->CHDATINR = (int16_t)pg_adc_buffer[i];
			hdfsdm1_channel1.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i];
			hdfsdm1_channel2.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i+1];
			hdfsdm1_channel3.Instance->CHDATINR = (int16_t)pg_accel_adc_buffer[i+2];
		}
	}
	else if (AdcHandle == &hadc2)
	{
		/* hadc2 doesn't use a circular DMA buffer so measurements can be retreived outside this interrupot handler */
		pg_measurement_done = true; 
	}		
}

void HAL_ADC_ErrorCallback(ADC_HandleTypeDef* AdcHandle)
{
	printf("ADC DMA error \r\n");
}

void HAL_DFSDM_FilterRegConvHalfCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm_filter) 
{
	int32_t i;

	if(hdfsdm_filter == &hdfsdm1_filter1)
	{
		for(i = 0; i < (DFSDM_DMA_BUFFER / 2); i++)
		{
			pg_filter_outputx[i + pg_dfsdm1_counter] = (int16_t)(pg_filter1_dma[i] >> DFSDM_BIT_SHIFT);
		}	
	}
	else if(hdfsdm_filter == &hdfsdm1_filter2)
	{
		for(i = 0; i < (DFSDM_DMA_BUFFER / 2); i++)
		{
			pg_filter_outputy[i + pg_dfsdm2_counter] = (int16_t)(pg_filter2_dma[i] >> DFSDM_BIT_SHIFT);
		}	
	}
	else
	{
		for(i = 0; i < (DFSDM_DMA_BUFFER / 2); i++)
		{
			pg_filter_outputz[i + pg_dfsdm3_counter] = (int16_t)(pg_filter3_dma[i] >> DFSDM_BIT_SHIFT);
		}	
	}
}

void HAL_DFSDM_FilterRegConvCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm_filter) 
{
	int32_t i; 
	
	if(hdfsdm_filter == &hdfsdm1_filter1)
	{
		for(i = DFSDM_DMA_BUFFER / 2; i < DFSDM_DMA_BUFFER; i++)
		{
			pg_filter_outputx[i + pg_dfsdm1_counter] = (int16_t)(pg_filter1_dma[i] >> DFSDM_BIT_SHIFT);
		}
		pg_dfsdm1_counter = pg_dfsdm1_counter + DFSDM_DMA_BUFFER;
		if(pg_dfsdm1_counter >= TOTAL_SAMPLES)
		{
			HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter1);
		}	
	}
	else if(hdfsdm_filter == &hdfsdm1_filter2)
	{
		for(i = DFSDM_DMA_BUFFER / 2; i < DFSDM_DMA_BUFFER; i++)
		{
			pg_filter_outputy[i + pg_dfsdm2_counter] = (int16_t)(pg_filter2_dma[i] >> DFSDM_BIT_SHIFT);
		}
		pg_dfsdm2_counter = pg_dfsdm2_counter + DFSDM_DMA_BUFFER;
		if(pg_dfsdm2_counter >= TOTAL_SAMPLES)
		{
			HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter2);
		}	
	}
	else
	{
		for(i = DFSDM_DMA_BUFFER / 2; i < DFSDM_DMA_BUFFER; i++)
		{
			pg_filter_outputz[i + pg_dfsdm3_counter] = (int16_t)(pg_filter3_dma[i] >> DFSDM_BIT_SHIFT);
		}
		pg_dfsdm3_counter = pg_dfsdm3_counter + DFSDM_DMA_BUFFER;
		if(pg_dfsdm3_counter >= TOTAL_SAMPLES)
		{
			HAL_DFSDM_FilterRegularStop_DMA(&hdfsdm1_filter3);
		}	
	}
}

void HAL_DFSDM_FilterErrorCallback(DFSDM_Filter_HandleTypeDef *hdfsdm_filter)
{
	uint8_t error;
	
	error = HAL_DFSDM_FilterGetError(hdfsdm_filter);
	printf("DFSDM error %d\r\n", error);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
