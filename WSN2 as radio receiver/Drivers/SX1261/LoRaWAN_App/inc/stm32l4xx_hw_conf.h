/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: contains hardaware configuration Macros and Constants

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
 /******************************************************************************
  * @file    stm32l4xx_hw_conf.h
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    10-July-2018
  * @brief   contains hardaware configuration Macros and Constants for stm32l4
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_CONF_L4_H__
#define __HW_CONF_L4_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

//#define RADIO_DIO_4
//#define RADIO_DIO_5

/* LORA I/O definition */

#define RADIO_RESET_PORT                          nLORA_RESET_GPIO_Port
#define RADIO_RESET_PIN                           nLORA_RESET_Pin

#define RADIO_MOSI_PORT                           LORA_MOSI_GPIO_Port
#define RADIO_MOSI_PIN                            LORA_MOSI_Pin

#define RADIO_MISO_PORT                           LORA_MISO_GPIO_Port
#define RADIO_MISO_PIN                            LORA_MISO_Pin

#define RADIO_SCLK_PORT                           LORA_SCK_GPIO_Port
#define RADIO_SCLK_PIN                            LORA_SCK_Pin

#define RADIO_NSS_PORT                            nLORA_CS_GPIO_Port
#define RADIO_NSS_PIN                             nLORA_CS_Pin

#define RADIO_BUSY_PORT                           LORA_BUSY_GPIO_Port
#define RADIO_BUSY_PIN                            LORA_BUSY_Pin

#define RADIO_DIO_1_PORT                          LORA_DIO1_GPIO_Port
#define RADIO_DIO_1_PIN                           LORA_DIO1_Pin
#define RADIO_DIO_1_EXTINT												EXTI15_10_IRQn

#define RADIO_LEDTX_PORT                          LED1_GPIO_Port
#define RADIO_LEDTX_PIN                           LED1_Pin

#define RADIO_LEDRX_PORT                          LED2_GPIO_Port
#define RADIO_LEDRX_PIN                           LED2_Pin


/*  SPI MACRO redefinition */

#define SPI_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()

#define SPI2_AF                          GPIO_AF5_SPI2  


/* --------------------------- RTC HW definition -------------------------------- */

#define RTC_OUTPUT       DBG_RTC_OUTPUT


/* --------------------------- DEBUG redefinition -------------------------------*/

#define __HAL_RCC_DBGMCU_CLK_ENABLE()
#define __HAL_RCC_DBGMCU_CLK_DISABLE()

#define LED_Toggle( x )
#define LED_On( x )
#define LED_Off( x )

#ifdef __cplusplus
}
#endif

#endif /* __HW_CONF_L4_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
