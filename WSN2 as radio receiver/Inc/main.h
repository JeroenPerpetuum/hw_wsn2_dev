/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define nTHERM_EN_Pin GPIO_PIN_13
#define nTHERM_EN_GPIO_Port GPIOC
#define LPUART_RX_Pin GPIO_PIN_0
#define LPUART_RX_GPIO_Port GPIOC
#define LPUART_TX_Pin GPIO_PIN_1
#define LPUART_TX_GPIO_Port GPIOC
#define ACCEL_Y__Pin GPIO_PIN_2
#define ACCEL_Y__GPIO_Port GPIOC
#define ACCEL_X__Pin GPIO_PIN_3
#define ACCEL_X__GPIO_Port GPIOC
#define THERM_V__Pin GPIO_PIN_0
#define THERM_V__GPIO_Port GPIOA
#define ACCEL_Z__Pin GPIO_PIN_1
#define ACCEL_Z__GPIO_Port GPIOA
#define PIEZO_X__Pin GPIO_PIN_2
#define PIEZO_X__GPIO_Port GPIOA
#define PIEZO_Y__Pin GPIO_PIN_3
#define PIEZO_Y__GPIO_Port GPIOA
#define nPIEZO_EN_Pin GPIO_PIN_4
#define nPIEZO_EN_GPIO_Port GPIOA
#define PGOOD_2V2_Pin GPIO_PIN_5
#define PGOOD_2V2_GPIO_Port GPIOA
#define MEM_IO3_Pin GPIO_PIN_6
#define MEM_IO3_GPIO_Port GPIOA
#define MEM_IO2_Pin GPIO_PIN_7
#define MEM_IO2_GPIO_Port GPIOA
#define VMON_STORE_Pin GPIO_PIN_4
#define VMON_STORE_GPIO_Port GPIOC
#define VMON_SPARE_Pin GPIO_PIN_5
#define VMON_SPARE_GPIO_Port GPIOC
#define MEM_IO1_Pin GPIO_PIN_0
#define MEM_IO1_GPIO_Port GPIOB
#define MEM_IO0_Pin GPIO_PIN_1
#define MEM_IO0_GPIO_Port GPIOB
#define MEM_EN_Pin GPIO_PIN_2
#define MEM_EN_GPIO_Port GPIOB
#define MEM_SCK_Pin GPIO_PIN_10
#define MEM_SCK_GPIO_Port GPIOB
#define nMEM_CS_Pin GPIO_PIN_11
#define nMEM_CS_GPIO_Port GPIOB
#define HARV_DETECT_Pin GPIO_PIN_12
#define HARV_DETECT_GPIO_Port GPIOB
#define LORA_SCK_Pin GPIO_PIN_13
#define LORA_SCK_GPIO_Port GPIOB
#define LORA_MISO_Pin GPIO_PIN_14
#define LORA_MISO_GPIO_Port GPIOB
#define LORA_MOSI_Pin GPIO_PIN_15
#define LORA_MOSI_GPIO_Port GPIOB
#define VMON_EN_Pin GPIO_PIN_6
#define VMON_EN_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOC
#define VERSION_BIT2_Pin GPIO_PIN_9
#define VERSION_BIT2_GPIO_Port GPIOC
#define nLORA_CS_Pin GPIO_PIN_8
#define nLORA_CS_GPIO_Port GPIOA
#define nLORA_RESET_Pin GPIO_PIN_9
#define nLORA_RESET_GPIO_Port GPIOA
#define LORA_BUSY_Pin GPIO_PIN_10
#define LORA_BUSY_GPIO_Port GPIOA
#define LORA_DIO1_Pin GPIO_PIN_11
#define LORA_DIO1_GPIO_Port GPIOA
#define LORA_DIO1_EXTI_IRQn EXTI15_10_IRQn
#define nLORA_EN_Pin GPIO_PIN_12
#define nLORA_EN_GPIO_Port GPIOA
#define nADXL372_EN_Pin GPIO_PIN_15
#define nADXL372_EN_GPIO_Port GPIOA
#define ADXL372_SCK_Pin GPIO_PIN_10
#define ADXL372_SCK_GPIO_Port GPIOC
#define ADXL372_MISO_Pin GPIO_PIN_11
#define ADXL372_MISO_GPIO_Port GPIOC
#define ADXL372_MOSI_Pin GPIO_PIN_12
#define ADXL372_MOSI_GPIO_Port GPIOC
#define nADXL372_CS_Pin GPIO_PIN_2
#define nADXL372_CS_GPIO_Port GPIOD
#define VERSION_BIT1_Pin GPIO_PIN_4
#define VERSION_BIT1_GPIO_Port GPIOB
#define ADXL372_IRQ1_Pin GPIO_PIN_5
#define ADXL372_IRQ1_GPIO_Port GPIOB
#define ADXL372_IRQ2_Pin GPIO_PIN_6
#define ADXL372_IRQ2_GPIO_Port GPIOB
#define VERSION_BIT0_Pin GPIO_PIN_8
#define VERSION_BIT0_GPIO_Port GPIOB
#define AVDD_EN_Pin GPIO_PIN_9
#define AVDD_EN_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
